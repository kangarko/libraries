Copyright Disclaimer:

Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, commenting, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favour of fair use.

We believe this repository is fair use because:

This is used for teaching and educational purposes in our open-source library Foundation found at https://github.com/kangarko/foundation where people can use the library and connect to these plugins without having to connect them manually, saving time. 

We also do not wish to use the heart of any piece of work that would perhaps decrease the market value of the original content, if anything we hope to promote the use of these plugins so that more people can use them and subsequently increase their market value.

If you have any questions, please contact us via legal@mineacademy.org.